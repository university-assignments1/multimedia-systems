import argparse
from MC import MC

def argparse_setup():
	
	global args
	global parser

	parser = argparse.ArgumentParser(description='Motion Compensation',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('--video', dest='video', required=True, metavar='path',
		help='Video file')
	parser.add_argument('--diffs', dest='write_diffs', default=False,
		action='store_true', help='Write prediction errors (Differences) in file')
	parser.add_argument('--mc', dest='write_mc_diffs', default=False,
		action='store_true', help='Write motion compensation in file')

	args = parser.parse_args()

argparse_setup()

if args.write_diffs:
	MC.Differences(args.video)
if args.write_mc_diffs:
	MC.MCDifferences(args.video)