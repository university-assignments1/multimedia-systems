import skvideo.io
import skvideo.utils
import scipy.stats

import math
import pandas as pd
import numpy as np
import progressbar


def entropy(array2d):

	gray_array = skvideo.utils.rgb2gray(array2d)
	p = pd.Series(gray_array.flatten())
	stat = p.value_counts()
	entropy = scipy.stats.entropy(stat, base=2)
	return entropy

def SAD(target_block, bc, reference_frame, i, j):
	y = bc[0]+i
	x = bc[1]+j

	n = target_block.shape[0]
	m = target_block.shape[1]

	reference_block = reference_frame[y:y+n, x:x+m, :]

	block_diffs = abs(target_block - reference_block)
	
	return sum(sum(sum(block_diffs)))

def pad_zeros(frame, block_size):

	rows = frame.shape[0]
	cols = frame.shape[1]

	channels = frame.shape[2]

	mod_rows = rows % block_size[0]
	mod_cols = cols % block_size[1]

	extra_rows = block_size[0] - (mod_rows) if mod_rows > 0 else 0
	extra_cols = block_size[1] - (mod_cols) if mod_cols > 0 else 0

	pad_rows = np.zeros((extra_rows, cols, channels), dtype=int)
	pad_cols = np.zeros((rows+extra_rows, extra_cols, channels), dtype=int)

	new_frame = np.r_[frame, pad_rows]
	new_frame = np.c_['1', new_frame, pad_cols]

	return new_frame

# BMA using exhaustive search
def best_match_block(target_block, reference_frame, bc, k):

	final_min = SAD(target_block, bc, reference_frame, 0, 0)
	final_coord = (0,0)

	for i in range(-k,k+1):

		if bc[0] + i < 0 or bc[0] + target_block.shape[0] + i > reference_frame.shape[0]: continue

		for j in range(-k,k+1):

			if bc[1] + j < 0 or bc[1] + target_block.shape[1] + j > reference_frame.shape[1]: continue

			temp_min = SAD(target_block, bc, reference_frame, i, j)

			if temp_min < final_min:
				final_min = temp_min
				final_coord = (i, j)

	return final_coord

# BMA using logarithmic search
def best_match_block_log(target_block, reference_frame, bc, k):

	if k == 0: return (0,0)

	final_min = SAD(target_block, bc, reference_frame, 0, 0)
	final_coord = (0,0)

	for i in range(-k, k + 1, k):

		if bc[0] + i < 0 or bc[0] + target_block.shape[0] + i > reference_frame.shape[0]: continue

		for j in range(-k, k + 1, k):

			if bc[1] + j < 0 or bc[1] + target_block.shape[1] + j > reference_frame.shape[1]: continue

			temp_min = SAD(target_block, bc, reference_frame, i, j)

			if temp_min < final_min:
				final_min = temp_min
				final_coord = (i,j)

	f = best_match_block_log(target_block, reference_frame, tuple(map(sum, zip(final_coord,bc))), k//2)

	return tuple(map(sum, zip(f, final_coord)))

def PredictedFrame(reference_frame, vectors, block_size):
	
	pred_frame = np.copy(reference_frame)

	# Vector index
	vi = 0

	for i in range(0, reference_frame.shape[0], block_size[0]):

		for j in range(0, reference_frame.shape[1], block_size[1]):

			block_position = (i+block_size[0], j+block_size[1])

			ref_block = reference_frame[
				i+vectors[vi][0] : block_position[0]+vectors[vi][0],
				j+vectors[vi][1] : block_position[1]+vectors[vi][1],
				:
			]

			pred_frame[i:block_position[0], j:block_position[1],:] = ref_block

			vi += 1

	return pred_frame

def MotionVectors(reference_frame, target_frame, block_size, k):
	"""
	Calculate and return the motion vectors.
	"""

	vectors = []

	for y in range(0, target_frame.shape[0], block_size[0]):

		for x in range(0, target_frame.shape[1], block_size[1]):

			target_block = target_frame[y:y+block_size[0], x:x+block_size[1], :]

			# Return the block x,y in the reference frame with the best match.
			bmb = best_match_block_log(target_block, reference_frame, (y,x), k)
			vectors.append(bmb)
	return vectors

class MC(object):


	@staticmethod
	def Differences(filename):
		"""
		Write prediction error.
		"""

		out_file = 'diffs_' + filename

		video_in = skvideo.io.FFmpegReader(filename)
		video_out = skvideo.io.FFmpegWriter(out_file)

		(FRAMES, HEIGHT, WIDTH, CHANNELS) = video_in.getShape()

		frame1 = video_in.__next__().astype(int)

		print("Export to file {}".format(out_file))

		bar = progressbar.ProgressBar(maxval=FRAMES, \
			widgets=[progressbar.Bar('=', '[', ']'), ' ',
			progressbar.Percentage()])
		bar.start()

		entropy_orig = entropy(frame1)
		entropy_diff = entropy(frame1)

		for i in range(FRAMES-1):

			bar.update(i+1)

			frame2 = video_in.__next__().astype(int)

			diffs = skvideo.utils.rgb2gray(255 - abs(frame2 - frame1))

			video_out.writeFrame(diffs)

			entropy_orig += entropy(frame2)
			entropy_diff += entropy(diffs)

			frame1 = frame2

		bar.finish()
		video_out.close()
		video_in.close()

		print("Original entropy: {:.4f}".format(entropy_orig/FRAMES))
		print("With prediction entropy: {:.4f}".format(entropy_diff/FRAMES))

	@staticmethod
	def MCDifferences(filename):
		
		out_file = 'mc_' + filename

		video_in = skvideo.io.FFmpegReader(filename)
		video_out = skvideo.io.FFmpegWriter(out_file)

		# Block size (typical size on MPEG)
		bsize = (16,16)
		# Search range
		k = 16

		(FRAMES, HEIGHT, WIDTH, CHANNELS) = video_in.getShape()

		frame1 = video_in.__next__().astype(int)
		frame1 = pad_zeros(frame1, bsize)

		print("Export to file {}".format(out_file))

		bar = progressbar.ProgressBar(maxval=FRAMES, \
			widgets=[progressbar.Bar('=', '[', ']'), ' ',
			progressbar.Percentage()])
		bar.start()

		entropy_orig = entropy(frame1)
		entropy_mc = entropy(frame1)

		for i in range(FRAMES-1):

			bar.update(i+1)

			frame2 = video_in.__next__().astype(int)
			frame2 = pad_zeros(frame2, bsize)

			# Calculate the motion vectors for frame1 and frame2.
			mv = MotionVectors(frame1, frame2, bsize, k)

			# Return the frame prediction.
			predicted_frame = PredictedFrame(frame1, mv, bsize)

			f = skvideo.utils.rgb2gray(255 - abs(predicted_frame - frame2))

			entropy_orig += entropy(frame2)
			entropy_mc += entropy(f)

			video_out.writeFrame(f.astype(np.uint8))

			frame1 = frame2

		bar.finish()
		video_in.close()
		video_out.close()

		print("Original entropy: {:.4f}".format(entropy_orig/FRAMES))
		print("With motion vectors entropy: {:.4f}".format(entropy_mc/FRAMES))
