import skvideo.io
import scipy.misc
import numpy as np
import progressbar
import math

import matplotlib.pyplot as plt
import skvideo.motion

import imp

# Load MC.py from Task_8_17 folder
f, path, desc = imp.find_module("MC", ["../Task_8_17"])
lib = imp.load_module("MC", f, path, desc)
MC = lib.MC


def getPlots(motionData):

	fig = plt.figure()
	plt.quiver(motionData[::-1, :, 0], motionData[::-1, :, 1])
	fig.axes[0].get_xaxis().set_visible(False)
	fig.axes[0].get_yaxis().set_visible(False)
	plt.tight_layout()
	fig.canvas.draw()

	# Get the RGBA buffer from the figure
	w,h = fig.canvas.get_width_height()
	buf = np.fromstring(fig.canvas.tostring_argb(), dtype=np.uint8)
	buf.shape = (h, w, 4)
	quiver = buf[:, :, 1:]
	plt.close()

	return quiver

class MR(object):

	@staticmethod
	def RemoveMotion(filename, bsize=(16,16), k=16):

		out_file = 'mr_' + filename

		video_in = skvideo.io.FFmpegReader(filename)
		video_out = skvideo.io.FFmpegWriter(out_file)

		(FRAMES, HEIGHT, WIDTH, CHANNELS) = video_in.getShape()

		print("frames: ", FRAMES)
		print("height: ", HEIGHT)
		print("width: ", WIDTH)


		frame1 = video_in.__next__()
		frame1 = lib.pad_zeros(frame1, bsize)
		prev_frame = frame1

		bar = progressbar.ProgressBar(maxval=FRAMES, \
			widgets=[progressbar.Bar('=', '[', ']'), ' ',
			progressbar.Percentage()])
		bar.start()

		for i in range(FRAMES-1):

			bar.update(i+1)

			frame2 = video_in.__next__()
			frame2 = lib.pad_zeros(frame2, bsize)
			
			mv = lib.MotionVectors(frame1, frame2, bsize, k)
			new_frame = np.copy(frame2)

			vi = -1
			for y in range(0, frame2.shape[0], bsize[0]):

				for x in range(0, frame2.shape[1], bsize[1]):

					vi += 1
					if mv[vi] == (0,0): continue

					block_position = (y+bsize[0], x+bsize[1])

					new_frame[y:block_position[0], x:block_position[1],:] = prev_frame[y:block_position[0], x:block_position[1],:]

			video_out.writeFrame(new_frame)

			frame1 = frame2
			prev_frame = np.copy(new_frame)

		bar.finish()

		video_out.close()
		video_in.close()

	@staticmethod
	def ShowMotionVectors(filename, bsize=(16,16), k=16):
		
		videodata = skvideo.io.vread(filename)

		T, M, N, C = videodata.shape

		motionData = skvideo.motion.blockMotion(videodata, mbSize=bsize[0], p=k)

		writer = skvideo.io.FFmpegWriter("motion.mp4")

		print("Motion vectors")

		bar = progressbar.ProgressBar(maxval=T, \
			widgets=[progressbar.Bar('=', '[', ']'), ' ',
			progressbar.Percentage()])
		bar.start()

		for i in range(T-1):
			bar.update(i+1)

			a = getPlots(motionData[i])
			frame = scipy.misc.imresize(videodata[i+1], (a.shape[0], a.shape[1], 3))

			outputframe = np.zeros((frame.shape[0], frame.shape[1]*2, 3), dtype=np.uint8)

			outputframe[:frame.shape[0], :frame.shape[1]] = frame
			outputframe[:frame.shape[0], frame.shape[1]:] = a

			writer.writeFrame(outputframe)

		writer.close()
		bar.finish()