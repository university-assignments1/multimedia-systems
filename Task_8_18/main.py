import argparse

def argparse_setup():
	
	global args
	global parser

	parser = argparse.ArgumentParser(description='Motion Detection and Removal',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('--video', dest='video', required=True, metavar='path',
		help='Video file')
	parser.add_argument('--vectors', dest='show_vectors', default=False,
		action='store_true', help='Write motion vectors in motion.mp4')
	parser.add_argument('--bsize', dest='bsize', default=(16,16), type=int, nargs=2, help='Set block size')
	parser.add_argument('-k', dest='k', default=16, type=int, help='Set search range')

	args = parser.parse_args()

argparse_setup()

from MR import MR

if args.show_vectors:
	MR.ShowMotionVectors(args.video, tuple(args.bsize), args.k)
else:
	MR.RemoveMotion(args.video, tuple(args.bsize), args.k)
