from skimage import io
import os
import numpy as np
import math


def bitstring_to_bytes(s):
	return int(s, 2).to_bytes(len(s) // 8, byteorder='big')

# Number of bits need for number n
def lg2_up(n):
	return math.floor(math.log2(n) + 1)

def CreatePrefix(height, width, q):
	h = "{0:b}".format(height).zfill(12)
	w = "{0:b}".format(width).zfill(12)
	q_b = "{0:08b}".format(q)

	return bitstring_to_bytes(q_b+h+w)

def ExtractPrefix(b):
	bitstream = ''.join(["{0:08b}".format(i) for i in b])

	q = bitstream[0:8]
	h = bitstream[8:20]
	w = bitstream[20:32]

	bitstream = bitstream[32:]

	return (int(h,2),int(w,2),int(q,2), bitstream)

def ReadRLE(filename):
	"""
	Read binary file and convert it to RLE list.

	This is for Decode() functionality.
	Creates the RLE list from the binary file.
	"""

	f = open(filename, 'rb')
	b = f.read()
	f.close()

	# Get height, width and q from prefix of the file.
	(h,w,q, bitstream) = ExtractPrefix(b)

	x_bits = lg2_up(w)
	y_bits = lg2_up(h)
	v_bits = lg2_up(np.round(255 / q).astype(int))

	# Remove possible padding
	padded_bits = len(bitstream) % (x_bits + y_bits + v_bits)
	if padded_bits != 0:
		bitstream = bitstream[:(-1)*(padded_bits)]

	rle_list = []
	i = 0
	while(i < len(bitstream)):
		y = int(bitstream[i:i+y_bits], 2)
		i += y_bits
		x = int(bitstream[i:i+x_bits], 2)
		i += x_bits
		v = int(bitstream[i:i+v_bits], 2)
		i += v_bits
		rle_list.append((y,x,v))

	return (rle_list, h, w, q)

def RLD(rle, x, y):
	"""
	Run Length Decoding and return 2d array.

	This is for Decode() functionality.
	Creates the 2d array image from RLE list.
	"""

	ret = []
	if len(rle) > 1:
		for i in range(len(rle)-1):
			current_x = rle[i][1]
			current_y = rle[i][0]
			current_v = rle[i][2]

			next_x = rle[i+1][1]
			next_y = rle[i+1][0]

			ret[current_y*x + current_x : next_y*x + next_x] = [rle[i][2]] * (next_y*x + next_x - current_y*x + current_x)

		ret[rle[i+1][0]*x + rle[i+1][1] : ] = [rle[i+1][2]] * (x*y - (rle[i+1][0]*x + rle[i+1][1]))
	else:
		ret[0 : x*y] = [rle[0][2]] * (x*y)

	return np.array([ret[i:i + x] for i in range(0, len(ret), x)])


def CompressionStats(u, c, o, out_file):

	compression_ratio = u / c

	print("=== Compression performance ===")
	print("before: {:.3f} KiB".format(u))
	print("after: {:.3f} KiB".format(c))
	print("other compression: {:.3f} KiB".format(o))
	print("Compression ratio: {:.2f}".format(u / c))
	print("Space Saving: {:.2f}%\n".format((1 - c / u) * 100))

	print("Writing file: {}\n".format(out_file))
	return (compression_ratio)

class Codec(object):

	@staticmethod
	def Quantization(array_2d, Q):
		array_2d = np.array(array_2d)
		return np.round(array_2d / Q).astype(int)

	@staticmethod
	def Dequantization(array_2d, Q):
		return array_2d * Q

	@staticmethod
	def RLE(array_2d):
		"""Run Length Encoding and return list of tuples"""

		last_value = -1
		rle_list = []
		for index_y, value_y in enumerate(array_2d):
			for index_x, value_x in enumerate(value_y):
				if last_value != value_x:
					rle_list.append((index_y, index_x, int(value_x)))
					last_value = value_x
		return rle_list

	@staticmethod
	def ReadImage(filename):
		"""Read image and return color channels as tuple of 2d arrays"""

		im = io.imread(filename)
		if len(im.shape) == 3:
			return ((im[:,:,0], im[:,:,1], im[:,:,2]), 'RGB')
		else:
			return (im, 'Gray')

	@staticmethod
	def RLEtoBytes(llist, max_x, max_y, max_v, q):
		"""Convert the RLE list into binary"""
		
		zfill_y = lg2_up(max_y)
		zfill_x = lg2_up(max_x)
		zfill_v = lg2_up(max_v)

		bitstream = ""
		for i in llist:
			bitstream +=	"{0:b}".format(i[0]).zfill(zfill_y) +\
					"{0:b}".format(i[1]).zfill(zfill_x)

			if i[2] < 0:
				v = "{0:b}".format(i[2]*(-1) + np.round(255 / q).astype(int)).zfill(zfill_v)
			else:
				v = "{0:b}".format(i[2]).zfill(zfill_v)

			bitstream += v


		# Fill with extra zeros to align with multiple of 8 bits.
		# We will remove that padding on ReadRLE()
		extra = len(bitstream) % 8
		bitstream += '0'*(8 - extra) if extra > 0 else ''

		return bitstring_to_bytes(bitstream)

	@staticmethod
	def CompressChannel(ch, q, v):
	
		c = Codec.Quantization(ch, q)
		r = Codec.RLE(c)
		b = Codec.RLEtoBytes(r, len(ch[0]), len(ch), np.round(255 / q).astype(int), q)

		if (v == True):
			print("=== Original channel ===")
			print(ch)
			print('\n')
			print("=== After quantization ===")
			print(c)
			print('\n')
			print("=== After run-length encoding ===")
			print(r)
			print('\n')
			print("=== Bytes to write for this channel ===")
			print(b)
			print('\n')

		return b

	@staticmethod
	def Decode(filename, outfile):
		"""
		Decode the given file and export the image.

		This method decodes the *filename*, make the reverse RLE and
		saves into file *outfile*.
		"""

		(rle_list, h, w, q) = ReadRLE(filename)
		pixels = RLD(rle_list, w, h)
		im = Codec.Dequantization(pixels, q)
		io.imsave(outfile, im.astype(np.uint8))

	@staticmethod
	def Encode(filename, q=10, v=False):
		"""
		Encoding the given image.

		This method does quantization in *filename* image with value of *q*,
		create the run-length encoding and stores the compressed image
		in file *out_file*. Το achieve this it uses other methods/functions
		declared in this file. 
		"""

		(CHANNEL, MODE) = Codec.ReadImage(filename)

		out_file = filename + '.out'

		# Check if image is grayscale...
		if MODE == 'Gray':

			shape = CHANNEL.shape

			c = Codec.CompressChannel(CHANNEL, q, v)

			unc_size = len(CHANNEL[0]) * len(CHANNEL) / 1024
			comp_size = len(c) / 1024

		# ...or RGB
		elif MODE == 'RGB':

			shape = CHANNEL[0].shape

			(RED, GREEN, BLUE) = (CHANNEL[0], CHANNEL[1], CHANNEL[2])

			c1 = Codec.CompressChannel(RED, q, v)
			c2 = Codec.CompressChannel(GREEN, q, v)
			c3 = Codec.CompressChannel(BLUE, q, v)

			c = c1 + c2 + c3

			unc_size =	len(RED[0]) * len(RED) / 1024 +\
					len(GREEN[0]) * len(GREEN) / 1024 +\
					len(BLUE[0]) * len(BLUE) / 1024

			comp_size = len(c1) / 1024 + len(c2) / 1024 + len(c3) / 1024

		else:
			print("Unknown color mode")
			return None

		# Calculate the prefix for extra info (height, width and q)
		pref = CreatePrefix(shape[0], shape[1], q)

		f = open(out_file, 'wb')
		f.write(pref + c)
		f.close()

		other_comp_size = os.stat(filename).st_size / 1024

		ratio = CompressionStats(unc_size, comp_size, other_comp_size, out_file)
		return (out_file, ratio)
