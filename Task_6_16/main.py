import argparse
import os

def argparse_setup():
	
	global args
	global parser

	parser = argparse.ArgumentParser(description='Encode and Decode images',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('-i', dest='folder', required=True, metavar='path', help='Folder or image')
	parser.add_argument('-q', dest='Q', default=10, type=int, help='Quantization value')
	parser.add_argument('-v', dest='verbose', default=False,
		action='store_true', help='Enable verbose mode (Warning do not use this on large images)')
	parser.add_argument('-d', dest='decode_flag', default=False, action='store_true',
		help="Enable the decode functionality for the input image/s.")

	args = parser.parse_args()

# First check if parameters are ok ...
argparse_setup()


# ... and then import the libraries
from Codec import Codec

total_ratio = 0
total_ratio_count = 0

# Encode
if not args.decode_flag:
	if os.path.isdir(args.folder):

		for filename in os.listdir(args.folder):

			if filename.endswith((".jpg", ".png", ".jpeg")):

				(out, ratio) = Codec.Encode(args.folder+'/'+filename, args.Q, args.verbose)

				total_ratio += ratio
				total_ratio_count += 1

		print("Average compression ratio: {:.2f}".format(total_ratio/total_ratio_count))

	elif os.path.isfile(args.folder):

		if args.folder.endswith((".jpg", ".png", ".jpeg")):

			(out, ratio) = Codec.Encode(args.folder, args.Q, args.verbose)
# Decode
else:

	if os.path.isdir(args.folder):

		for filename in os.listdir(args.folder):

			if filename.endswith(".out"):

				Codec.Decode(args.folder+'/'+filename,
					args.folder+'/'+filename+'_decoded.png')

	elif os.path.isfile(args.folder):

		if args.folder.endswith(".out"):

			Codec.Decode(args.folder, args.folder+'_decoded.png')