import argparse

def argparse_setup():
	
	global args
	global parser

	parser = argparse.ArgumentParser(description='Video Compression using DPCM-like algorithm',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	
	parser.add_argument('--video', dest='input_video', required=True,
		metavar='path', help='The path of the video/file to encode/decode')
	parser.add_argument('-q', dest='Q', default=10, type=int, help='Quantization value')
	parser.add_argument('-d', dest='decode_flag', default=False, action='store_true',
		help="Enable the decode functionality for the given compressed video.")
	parser.add_argument('-c', dest='codec_encoding', default=False, action='store_true',
		help="Compress the video (frame by frame) using the Codec-Image compression.")

	args = parser.parse_args()

argparse_setup()

from DPCM import DPCM

if not args.decode_flag:
	DPCM.Encode(args.input_video, args.Q, args.codec_encoding)
else:
	DPCM.Decode(args.input_video, args.Q)
