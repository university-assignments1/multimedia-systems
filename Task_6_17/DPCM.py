import numpy as np
import skvideo.io
import os
import math
import imp
import progressbar

# Load Codec.py from Task_6_16 folder
f, path, desc = imp.find_module("Codec", ["../Task_6_16"])
lib = imp.load_module("Codec", f, path, desc)
Codec = lib.Codec


def ReadChannelRLE(f, y_bits, x_bits, v_bits, max_v):
	
	bytes_to_read = math.ceil((y_bits + x_bits + v_bits) / 8)
	rle_list = []
	
	eof = False

	rle_empty = True

	while True:
		b = f.read(bytes_to_read)

		# END of file
		if not b:
			eof = True
			break

		bitstream = ''.join(["{0:08b}".format(i) for i in b])

		# Extract the rle values from bitstream
		y = int(bitstream[0:y_bits], 2)
		x = int(bitstream[y_bits:y_bits+x_bits], 2)
		v = int(bitstream[y_bits+x_bits:y_bits+x_bits+v_bits], 2)

		if v > max_v:
			v = (v - max_v) * (-1)

		# End of frame
		if y == 0 and x == 0 and not rle_empty:
			f.seek((-1)*bytes_to_read, 1)
			break
		else:
			rle_empty = False

		rle_list.append((y,x,v))

	return (rle_list, eof)



class DPCM(object):
	
	@staticmethod
	def Encode(filename, q=10, use_codec_encoding=False):
		"""
		Compress the given video using DPCM.

		Find the differences from frame i and i-1, then run quantization
		on the result after that calculate the RLE list and finally
		save the binary result into the *filename*.out.
		"""

		video = skvideo.io.FFmpegReader(filename)
		shape = video.getShape()

		(FRAMES, HEIGHT, WIDTH, CHANNELS) = shape

		print("Number of frames: {}".format(shape[0]))
		print("Height: {}".format(HEIGHT))
		print("Width: {}\n".format(WIDTH))

		frame1 = np.zeros(shape=(HEIGHT, WIDTH, CHANNELS), dtype=np.int8)

		f = open(filename + '.out', 'wb')

		# Write the information needed for decode (prefix)
		pref = lib.CreatePrefix(HEIGHT, WIDTH, q)

		# Also write the number of frames in video (max 2^32 frames)
		f.write(pref+lib.bitstring_to_bytes("{0:032b}".format(FRAMES)))

		comp_size = 0

		# set the size of bits for values y,x,v
		block_size = 2 ** (math.ceil(lib.lg2_up(max(WIDTH, HEIGHT, 8)) / 8) * 8) - 1

		bar = progressbar.ProgressBar(maxval=FRAMES, \
			widgets=[progressbar.Bar('=', '[', ']'), ' ',
			progressbar.Percentage()])
		bar.start()

		# For each frame find the difference with previous,
		# quantization, find the RLE list and save the result.
		if use_codec_encoding:

			for i in range(FRAMES):

				bar.update(i+1)
				frame2 = video.__next__()
				
				qframe2 = Codec.Quantization(frame2, q)

				for y in range(CHANNELS):

					rle_list = Codec.RLE(qframe2[:,:,y])
					# Set max_v = 65535 to force store v in 16 bits.
					bframe = Codec.RLEtoBytes(rle_list, block_size, block_size, 65535, q)
					comp_size += len(bframe)
					f.write(bframe)

		else:

			for i in range(FRAMES):

				bar.update(i+1)
				frame2 = video.__next__()

				diffs = frame2.astype(int) - frame1.astype(int)
				qdiffs = Codec.Quantization(diffs, q)

				frame1 = frame2

				for y in range(CHANNELS):

					rle_list = Codec.RLE(qdiffs[:,:,y])
					# Set max_v = 65535 to force store v in 16 bits.
					bframe = Codec.RLEtoBytes(rle_list, block_size, block_size, 65535, q)
					comp_size += len(bframe)
					f.write(bframe)

		f.close()
		bar.finish()

		print('\n')

		unc_size = FRAMES * HEIGHT * WIDTH * CHANNELS / 1024
		comp_size /= 1024
		other_comp_size = os.stat(filename).st_size / 1024

		lib.CompressionStats(unc_size, comp_size, other_comp_size,
			filename + '.out')

	@staticmethod
	def Decode(out_file, q):

		f = open(out_file, 'rb')
		writer = skvideo.io.FFmpegWriter(out_file+'_decoded.mp4')

		# Magic number of bytes to read (12 + 12 + 8 bits).
		bytes_pref = f.read(4)
		(h,w,q,_) = lib.ExtractPrefix(bytes_pref)

		bytes_frames = f.read(4)
		FRAMES = int.from_bytes(bytes_frames, 'big')

		block_size = math.ceil(lib.lg2_up(max(w, h, 8)) / 8) * 8

		x_bits = block_size
		y_bits = block_size
		v_bits = 16

		eof = False

		prev_frame = np.zeros(shape=(h,w,3), dtype=int)

		bar = progressbar.ProgressBar(maxval=FRAMES, \
			widgets=[progressbar.Bar('=', '[', ']'), ' ',
			progressbar.Percentage()])
		bar.start()

		i=0
		maxv = np.round(255 / q).astype(int)

		while not eof:

			bar.update(i+1)
			i+=1

			(rle_list1, _) = ReadChannelRLE(f, y_bits, x_bits, v_bits, maxv)
			ch1array = lib.RLD(rle_list1, w, h)

			(rle_list2, _) = ReadChannelRLE(f, y_bits, x_bits, v_bits, maxv)
			ch2array = lib.RLD(rle_list2, w, h)

			(rle_list3, eof) = ReadChannelRLE(f, y_bits, x_bits, v_bits, maxv)
			ch3array = lib.RLD(rle_list3, w, h)

			frame = np.dstack([ch1array, ch2array, ch3array])

			deq_frame = Codec.Dequantization(frame, q)
			deq_frame[deq_frame == -260] = -255
			deq_frame[deq_frame == 260] = 255

			diff = deq_frame + prev_frame

			diff[diff > 255] = 255
			diff[diff < 0] = 0

			prev_frame = diff

			writer.writeFrame(diff.astype(np.uint8))

		bar.finish()
		f.close()
		writer.close()